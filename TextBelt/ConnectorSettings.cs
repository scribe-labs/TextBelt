﻿namespace TextBeltConnector
{
    /// <summary>
    ///     Used for Discovery. These constants are used to fill in some of the values in the
    ///     ScribeConnectorAttrribute declaration on the class that implements IConnector.
    ///     Connector settings that are often or must different from other connectors.
    /// </summary>
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "{7BE487CA-B0E3-4DA4-97F5-46224DF5F224}";

        public const string ConnectorVersion = "1.0.1";

        public const string Description = "Scribe Labs Connector for TextBelt SMS Service (http://textbelt.com/)";

        public const string Name = "Scribe Labs – TextBelt";

        public const bool SupportsCloud = false;

        public const string CompanyName = "Scribe Software Corporation";

        public const string AppName = "Scribe Labs – TextBelt";

        public const string Copyright = "Copyright © 2015 Scribe All rights reserved.";
    }
}