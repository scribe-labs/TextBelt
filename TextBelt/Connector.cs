﻿namespace TextBeltConnector
{
    using Simple.Connector.Framework;
    using Simple.Connector.Framework.Http;

    [SimpleConnector(ConnectorSettings.ConnectorTypeId, ConnectorSettings.Name, ConnectorSettings.Description,
        typeof(Connector), ConnectorSettings.SupportsCloud, ConnectorSettings.ConnectorVersion)]
    public class Connector : HttpConnectorBase<TextBeltConnectorConnectionInfo>
    {
        public Connector()
            : base(ConnectorSettings.ConnectorTypeId, ConnectorSettings.CompanyName, "http://www.scribesoft.com", "{FE8BB096-A55F-4FDA-B4C1-3E1102B27126}")
        {
        }
    }
}