﻿namespace TextBeltConnector
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    using Simple.Connector.Framework;
    using Simple.Connector.Framework.Http;
    //using ScribeLabs.Simple.Connector.Framework.Http.Json.Net;

    //using POCNextCallerConnectorPoC.Data;
    using TextBeltConnector.Extensions;
    using System.Text;

    public class TextBeltConnectorConnectionInfo : HttpClientConnectionInfoBase<TextBeltConnectorConnectionInfo>
    {
        private ISerializer newtonserializer;

        public string BaseUrl { get; set; }


        //[IgnoreDataMember]
        //public override ISerializer Serializer
        //{
        //    get
        //    {
        //        if (this.newtonserializer == null)
        //        {
        //            var converters = new List<JsonConverter>();
        //            converters.Add(new LenientDateConverter());
        //            // converters.Add(new EmptyBoolConverter());
        //            this.newtonserializer = new JSonSerializer(converters);
        //        }
        //        return this.newtonserializer;
        //    }
        //}
        
        protected override Func<TextBeltConnectorConnectionInfo, IConnectionConfiguration> ConnectionConfiguration()
        {

            //string LoginUrl = "http://textbelt.com/text";
            string LoginUrl = BaseUrl;
            return
                this.Connection.Configure("POST", connInfo => LoginUrl)
                    .ConnectionInfoToBaseUrl(info => info.BaseUrl)
                    .ToHeader("Accept", "application/json")
                    .End();
        }

        protected override IList<HttpQueryRegistration> ConfigureQueries()
        {


            return new List<HttpQueryRegistration> {  };
        }


        protected override IList<HttpCallDescription> ConfigureOperations()
        {

            var sms = new HttpCallDescription("SMSSend", "SMSSend", "Create", "", "POST", this.Serializer)
                    .FromInputToForm("number")
                    .FromInputToForm("message")
                    .FromResponse<SMSSend>()
                    .AcceptJson();

            return new List<HttpCallDescription> { sms };
        }
    }
}