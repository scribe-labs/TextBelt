﻿namespace TextBeltConnector.Extensions
{
    using System;

    using Newtonsoft.Json;

    public class LenientDateConverter : JsonConverter
    {
        //private bool canWrite;

        public override bool CanWrite { get { return false; } }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(
            JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer)
        {
            var t = objectType;

            if (reader.TokenType == JsonToken.String)
            {
                try
                {
                    var str = reader.Value as string;

                    var dt = DateTime.Parse(str);
                    var obj = existingValue;
                    return dt;
                } catch
                {
                }
            }

            return DateTime.MinValue;
        }

        public override bool CanConvert(Type objectType) { return objectType == typeof(DateTime); }
    }
}