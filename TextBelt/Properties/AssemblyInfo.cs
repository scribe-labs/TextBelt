﻿using System.Reflection;
using System.Runtime.InteropServices;

using TextBeltConnector;

[assembly: AssemblyTitle(ConnectorSettings.CompanyName + "." + "Connector" + "." + ConnectorSettings.AppName)]
[assembly: AssemblyDescription(ConnectorSettings.Description)]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(ConnectorSettings.CompanyName)]
[assembly: AssemblyProduct(ConnectorSettings.CompanyName + "." + "Connector" + "." + ConnectorSettings.AppName)]
[assembly: AssemblyCopyright(ConnectorSettings.Copyright)]
[assembly: ComVisible(false)]

// We should make sure that this works with our build system - They should be responsible for Major.Minor.XXX.* we should replace * with a build numnber.

[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]